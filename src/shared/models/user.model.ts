import { ROLES } from '../enums/roles.enum';

export class User {
  private _id : number;
  public get id() : number {
    return this._id;
  }
  public set id(v : number) {
    this._id = v;
  }

  private _name : string;
  public get name() : string {
    return this._name;
  }
  public set name(v : string) {
    this._name = v;
  }

  private _role : string;
  public get role() : string {
    return this._role;
  }
  public set role(v : string) {
    this._role = v;
  }
  public get is_admin(): boolean {
    return this.role && this.role === ROLES.ADMIN;
  }
  public get is_user(): boolean {
    return this.role && this.role === ROLES.USER;
  }

  constructor(d?: any) {
    if (d) {
      this.id = d.id;
      this.name = d.name;
      this.role = d.role;
    }
  }
}
