import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

import { JwtHelper } from '../utils/jwt.helper';
import { User } from '../models/user.model';

export interface SessionState {
  token: string;
  user: User;
}

export function createInitialState(): SessionState {
  return {
    token: '',
    user: new User(),
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {

  constructor() {
    super(createInitialState());
  }

  login(session: SessionState) {
    const user = new User(JwtHelper.decodeToken(session.token).context);
    const newSession = this.createSession(session.token, user);

    this.update(newSession);

    localStorage.setItem('akita_app', JSON.stringify(session));
  }

  logout() {
    localStorage.removeItem('akita_app');
    this.update(createInitialState());
  }

  createSession(token: string, user: User) {
    return { token, user };
  }
}

