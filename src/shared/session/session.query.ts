import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SessionStore, SessionState } from './session.store';
import { JwtHelper } from '../utils/jwt.helper';
import { User } from '../models/user.model';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends Query<SessionState> {
  public isLoggedIn$ = this.select(session => {
    return !!session.token && !JwtHelper.isTokenExpired(session.token);
  });

  constructor(protected store: SessionStore) {
    super(store);
  }

  fetchLocalStorageData() {
    const data = JSON.parse(localStorage.getItem('akita_app'));

    if (data && data.hasOwnProperty('token')) {
      const user = new User(JwtHelper.decodeToken(data.token).context);

      const newSession = this.store.createSession(data.token, user);
      this.store.update(newSession);
    }
  }
}
