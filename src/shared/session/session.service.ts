import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
// import { HttpClient } from '@angular/common/http';

import { SessionStore } from './session.store';
import { LoginData } from '../interfaces/login-data.interface';
import { SessionDataService } from './session-data.service';

@Injectable({ providedIn: 'root' })
export class SessionService {

  constructor(
    private sessionStore: SessionStore,
    private _router: Router,
    private _authService: SessionDataService,
    // private http: HttpClient
  ) {}

  login(loginData: LoginData): Observable<any> {
    if (!loginData.email) {
      return throwError('token is wrong');
    }

    return this._authService.login(loginData)
    .pipe(
      tap(res => {
        this.sessionStore.login(res)
        this._router.navigate(['/todos']);
      })
    )
  }

  logout(): Observable<any> {
    return this._authService.logout()
    .pipe(
      tap(res => {
        this.sessionStore.logout();
      })
    )
  }
}
