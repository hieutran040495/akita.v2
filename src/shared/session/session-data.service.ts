import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, timer  } from 'rxjs';
import { mapTo } from 'rxjs/operators';

import { SessionStore } from './session.store';
import { LoginData } from '../interfaces/login-data.interface';

@Injectable({ providedIn: 'root' })
export class SessionDataService {

  constructor(
  ) {}

  login(loginData: LoginData): Observable<any> {
    return timer(300).pipe(mapTo({
      token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLnNvZC5ibGEtb25lLm5ldC9zdG9yZS9hcGkvdjEvYXV0aC9hZG1pbi9sb2dpbiIsImlhdCI6MTU0NDUxMzcxNCwiZXhwIjoxNTQ1MTE4NTE0LCJuYmYiOjE1NDQ1MTM3MTQsImp0aSI6IjFWVGhMaWZ5U2VDQkJyalQiLCJzdWIiOjgwMjg3LCJwcnYiOiI1ODQ5Y2MwNzk3ZTI1OWVhMjkyNTRkOGMwMTA3ODdjMmZlMDE1MTQ4IiwiY29udGV4dCI6eyJuYW1lIjoia2FrYWthIiwicm9sZSI6ImFkbWluIiwiaWQiOjgwMjg3fX0.amOc6i8dCgcGYn4UI3CxyZYCRNeq4-Ud4Z5WWVgbTJk'
    }));
  }

  logout() {
    return timer(300).pipe(mapTo({
      status: 200,
    }));
  }
}
