import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionQuery } from 'src/shared/session';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private _router: Router,
    private _sessionQuery: SessionQuery,
    ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean | Observable<boolean> {
    this._sessionQuery.isLoggedIn$.subscribe(isLoggedIn => {
      if (isLoggedIn) {
        // todo loggedin
        return;
      }
      this._router.navigate(['/login']);
    });

    return this._sessionQuery.isLoggedIn$;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(next, state);
  }
}
