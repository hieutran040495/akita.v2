import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HerosComponent } from './heros.component';
// import { ListOfHerosComponent} from './list-of-heros/list-of-heros.component';

// import { TodoService } from '../../../shared/services/todo.service';
// import { HeroService } from '../../../shared/services/hero.service';

const routes: Routes = [
  {
    path: '',
    component: HerosComponent,
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    HerosComponent,
    // ListOfHerosComponent,
  ],
  providers: [
    // TodoService,
    // HeroService,
  ]
})
export class HerosModule { }
