import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PagesComponent } from './pages.component';
import { ModalLogoutComponent } from './modal-logout/modal-logout.component'

import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'todos',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: '.\/todos\/todos.module#TodosModule'
      },
      {
        path: 'heros',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: '.\/heros\/heros.module#HerosModule'
      },
      {
        path: '**',
        redirectTo: 'todos',
        pathMatch: 'full'
      },
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
  ],
  declarations: [
    PagesComponent,
    ModalLogoutComponent,
  ],
  entryComponents: [
    ModalLogoutComponent,
  ]
})
export class PagesModule { }
