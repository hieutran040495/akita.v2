import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { TodosComponent } from './todos.component';
// import { TodoService } from '../../../shared/services/todo.service';

const routes: Routes = [
  {
    path: '',
    component: TodosComponent,
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    TodosComponent,
  ],
  providers: [
    // TodoService,
  ]
})
export class TodosModule { }
