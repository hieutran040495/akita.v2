import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { SessionService, SessionQuery } from '../../../shared/session';

@Component({
  selector: 'app-modal-logout',
  templateUrl: './modal-logout.component.html',
  styleUrls: ['./modal-logout.component.scss']
})
export class ModalLogoutComponent implements OnInit {

  constructor(
    private _sessionService: SessionService,
    private _sessionQuery: SessionQuery,
    private _bsModalRef: BsModalRef,
    private _modalSv: BsModalService,
  ) { }

  ngOnInit() {
  }

  public logout() {
    this._sessionService.logout()
    .subscribe(res => {
      this.closeModal();
    }, errors => {
      alert(errors);
    });
  }

  public closeModal(reason?: string) {
    if (reason) {
      this._modalSv.setDismissReason(reason);
    }
    this._bsModalRef.hide();
  }
}
