import { Component, OnInit } from '@angular/core';

import { SessionService } from '../../../shared/session/session.service';
import { LoginData } from 'src/shared/interfaces/login-data.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginData: LoginData = {
    email: '',
    password: '',
  };

  constructor(
    private _sessionService: SessionService,
  ) { }

  ngOnInit() {
  }

  public login() {
    this._sessionService.login(this.loginData)
    .subscribe(res => {
      console.log('login success');
    }, errors => {
      alert(errors);
    });

  }
}
