import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ModalLogoutComponent } from './modal-logout/modal-logout.component';
import { SessionQuery } from 'src/shared/session';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  public isAdmin: boolean = false;

  constructor(
    private _bsModalService: BsModalService,
    private _sessionQuery: SessionQuery,
  ) { }

  ngOnInit() {
    this.isAdmin = this._sessionQuery.getSnapshot().user.is_admin;
  }

  public openModalLogout() {
    const initData = {
      data: 'hello'
    };

    this._openModalWithComponent(ModalLogoutComponent, initData);
  }

  private _openModalWithComponent(comp, initialState: any = {}) {
    const subscribe = this._bsModalService.onHidden.subscribe((reason: any) => {
      subscribe.unsubscribe();
    });

    this._bsModalService.show(comp, { initialState });
  }
}
