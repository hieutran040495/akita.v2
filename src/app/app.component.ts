import { Component, OnInit } from '@angular/core';
import { SessionQuery } from 'src/shared/session';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private _sessionQuery: SessionQuery,
  ) {}

  ngOnInit() {
    this._sessionQuery.isLoggedIn$.subscribe(isLoggedIn => {
      if (!isLoggedIn) {
        this._sessionQuery.fetchLocalStorageData();
      }
    });
  }
}
